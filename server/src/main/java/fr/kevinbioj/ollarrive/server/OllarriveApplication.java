package fr.kevinbioj.ollarrive.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OllarriveApplication {

	public static void main(String[] args) {
		SpringApplication.run(OllarriveApplication.class, args);
	}

}
